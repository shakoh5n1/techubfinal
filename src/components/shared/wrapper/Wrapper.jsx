import React from 'react';
import './Wrapper.css';

export default class WrapperComponent extends React.Component {
    render() {
        return (
            <div className="wrapper">
                {this.props.children}
            </div>
        )
    }
}