import React from 'react';
import './Header.css';
import WrapperComponent from '../shared/wrapper/Wrapper';

export default class HeaderComponent extends React.Component {
    render() {
        return (
            <header className="header">
                <WrapperComponent>
                    shako
                </WrapperComponent>
            </header>
        )
    }
}