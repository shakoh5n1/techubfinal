import React from 'react';
import HeaderComponent from '../../components/header/Header';

export default class HomePage extends React.Component {
    render() {
        return (
            <React.Fragment>
                <HeaderComponent />
            </React.Fragment>
        )
    }
}